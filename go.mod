module gitlab.com/ujjalbuet/mem

require (
	github.com/golang/mock v1.3.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/onsi/ginkgo v1.8.0
	github.com/onsi/gomega v1.5.0
	github.com/rs/xid v1.2.1
	gitlab.com/akita/akita v1.4.1
	gitlab.com/akita/util v0.1.8
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7 // indirect
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect

)

go 1.13
