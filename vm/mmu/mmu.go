package mmu

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/util/akitaext"
	"gitlab.com/akita/util/ca"
)

// An MMU interface is the interface exposed to the driver and the emulator to
// allocate memory spaces and translate from virtual address to physical
// address.
type MMU interface {
	CreatePage(page *vm.Page)
	RemovePage(pid ca.PID, addr uint64)
	Translate(pid ca.PID, vAddr uint64) (uint64, *vm.Page)
	GetOrCreatePageTable(pid ca.PID) vm.PageTable
	InvalidatePage(vAddr uint64, pid ca.PID)
	ValidatePage(vAddr uint64, pid ca.PID)
	GetNumPages(pid ca.PID) int
}

type transaction struct {
	req       *vm.TranslationReq
	page      *vm.Page
	cycleLeft int
	migration *vm.PageMigrationReqToDriver
}

// MMUImpl is the default mmu implementation. It is also an akita Component.
type MMUImpl struct {
	akitaext.TickingComponent

	pageTableFactory vm.PageTableFactory

	ToTop                    akita.Port
	MigrationPort            akita.Port
	MigrationServiceProvider akita.Port
	topSender                akitaext.BufferedSender

	latency             int
	maxRequestsInFlight int
	numRequestsInFlight int
	PageTables          map[ca.PID]vm.PageTable

	walkingTranslations []transaction
	currentMigration    transaction
	isDoingMigration    bool
}

// CreatePage add a page to the page table.
func (mmu *MMUImpl) CreatePage(page *vm.Page) {
	table := mmu.GetOrCreatePageTable(page.PID)
	table.InsertPage(page)
}

// RemovePage deletes the page contains the target VAddr
func (mmu *MMUImpl) RemovePage(pid ca.PID, vAddr uint64) {
	table := mmu.GetOrCreatePageTable(pid)
	table.RemovePage(vAddr)
}

// GetNumPages returns the number of page table entries for a given PID
func (mmu *MMUImpl) GetNumPages(pid ca.PID) int {
	return mmu.PageTables[pid].GetNumPages()
}

// Translate convert virtual address to physical address.
func (mmu *MMUImpl) Translate(pid ca.PID, vAddr uint64) (uint64, *vm.Page) {
	table := mmu.GetOrCreatePageTable(pid)
	page := table.FindPage(vAddr)
	if page == nil || page.Valid == false {
		return 0, nil
	}

	offset := vAddr % page.PageSize
	pAddr := page.PAddr + offset
	return pAddr, page
}

// Tick defines how the MMU update state each cycle
func (mmu *MMUImpl) Tick(now akita.VTimeInSec) bool {
	madeProgress := false

	madeProgress = mmu.topSender.Tick(now) || madeProgress
	madeProgress = mmu.walkPageTable(now) || madeProgress
	madeProgress = mmu.processMigrationReturn(now) || madeProgress
	madeProgress = mmu.parseFromTop(now) || madeProgress

	return madeProgress
}

func (mmu *MMUImpl) trace(now akita.VTimeInSec, what string) {
	ctx := akita.HookCtx{
		Domain: mmu,
		Now:    now,
		Item:   what,
	}

	mmu.InvokeHook(&ctx)
}

func (mmu *MMUImpl) walkPageTable(now akita.VTimeInSec) bool {
	madeProgress := false
	for i, walking := range mmu.walkingTranslations {
		if mmu.walkingTranslations[i].cycleLeft > 0 {
			mmu.walkingTranslations[i].cycleLeft--
			madeProgress = true
			continue
		}

		madeProgress = mmu.finalizePageWalk(now, walking) || madeProgress
	}
	return madeProgress
}

func (mmu *MMUImpl) finalizePageWalk(
	now akita.VTimeInSec,
	walking transaction,
) bool {
	req := walking.req
	table := mmu.GetOrCreatePageTable(req.PID)
	page := table.FindPage(req.VAddr)
	walking.page = page

	if page == nil || page.Valid == false {
		panic("page not found")
	}

	if page.IsMigrating {
		return false
	}

	if mmu.pageNeedMigrate(walking) {
		return mmu.doMigration(now, walking)
	}

	return mmu.doPageWalkHit(now, walking)
}

func (mmu MMUImpl) pageNeedMigrate(walking transaction) bool {
	if walking.req.GPUID == walking.page.GPUID {
		return false
	}

	if !walking.page.Unified {
		return false
	}

	return true
}

func (mmu *MMUImpl) doPageWalkHit(
	now akita.VTimeInSec,
	walking transaction,
) bool {
	if !mmu.topSender.CanSend(1) {
		return false
	}

	rsp := vm.TranslationRspBuilder{}.
		WithSendTime(now).
		WithSrc(mmu.ToTop).
		WithDst(walking.req.Src).
		WithRspTo(walking.req.ID).
		WithPage(*walking.page).
		Build()

	mmu.topSender.Send(rsp)
	mmu.numRequestsInFlight--
	mmu.removeWalkingTranslation(walking)
	return true
}

func (mmu *MMUImpl) doMigration(
	now akita.VTimeInSec,
	walking transaction,
) bool {
	if mmu.isDoingMigration {
		return false
	}

	req := vm.NewPageMigrationReqToDriver(
		now, mmu.MigrationPort, mmu.MigrationServiceProvider)
	req.PhysicalAddress = walking.page.PAddr
	req.VAddr = walking.page.VAddr
	req.PageSize = walking.page.PageSize
	req.PID = walking.page.PID
	req.RequestingGPU = walking.req.GPUID
	err := mmu.MigrationPort.Send(req)
	if err != nil {
		return false
	}

	walking.page.IsMigrating = true
	walking.migration = req
	mmu.isDoingMigration = true
	mmu.currentMigration = walking
	mmu.removeWalkingTranslation(walking)
	return true
}

func (mmu *MMUImpl) removeWalkingTranslation(walking transaction) {
	for i, w := range mmu.walkingTranslations {
		if w.req.ID == walking.req.ID {
			mmu.walkingTranslations = append(
				mmu.walkingTranslations[:i],
				mmu.walkingTranslations[i+1:]...)
		}
	}
}

func (mmu *MMUImpl) processMigrationReturn(now akita.VTimeInSec) bool {
	item := mmu.MigrationPort.Peek()
	if item == nil {
		return false
	}

	if !mmu.topSender.CanSend(1) {
		return false
	}

	req := mmu.currentMigration.req
	table := mmu.GetOrCreatePageTable(req.PID)
	page := table.FindPage(req.VAddr)
	mmu.currentMigration.page = page

	rsp := vm.TranslationRspBuilder{}.
		WithSendTime(now).
		WithSrc(mmu.ToTop).
		WithDst(req.Src).
		WithRspTo(req.ID).
		WithPage(*page).
		Build()
	mmu.topSender.Send(rsp)

	page.IsMigrating = false
	mmu.isDoingMigration = false
	mmu.numRequestsInFlight--
	mmu.MigrationPort.Retrieve(now)

	return true
}

func (mmu *MMUImpl) parseFromTop(now akita.VTimeInSec) bool {
	if mmu.numRequestsInFlight >= mmu.maxRequestsInFlight {
		return false
	}

	req := mmu.ToTop.Retrieve(now)
	if req == nil {
		return false
	}

	switch req := req.(type) {
	case *vm.TranslationReq:
		mmu.startWalking(req)
	default:
		log.Panicf("MMU canot handle request of type %s", reflect.TypeOf(req))
	}

	mmu.numRequestsInFlight++
	return true
}

func (mmu *MMUImpl) startWalking(req *vm.TranslationReq) {
	translationInPipeline := transaction{
		req:       req,
		cycleLeft: mmu.latency,
	}

	mmu.walkingTranslations = append(mmu.walkingTranslations, translationInPipeline)
}

func (mmu *MMUImpl) InvalidatePage(vAddr uint64, pid ca.PID) {
	table := mmu.GetOrCreatePageTable(pid)

	page := table.FindPage(vAddr)
	if page == nil {
		log.Panicf("Trying to invalidate a page that does not exist")

	}

	page.Valid = false

}

func (mmu *MMUImpl) ValidatePage(vAddr uint64, pid ca.PID) {
	table := mmu.GetOrCreatePageTable(pid)

	page := table.FindPage(vAddr)
	if page == nil {
		log.Panicf("Trying to valid a page that does not exist")

	}

	page.Valid = true

}

func (mmu *MMUImpl) GetOrCreatePageTable(pid ca.PID) vm.PageTable {
	table, found := mmu.PageTables[pid]

	if !found {
		table = mmu.pageTableFactory.Build()
		mmu.PageTables[pid] = table
	}

	return table
}
