package mmu

import (
	"gitlab.com/akita/akita"
	"gitlab.com/akita/mem/vm"
	"gitlab.com/akita/util"
	"gitlab.com/akita/util/akitaext"
	"gitlab.com/akita/util/ca"
)

// A Builder can build MMU component
type Builder struct {
	engine                   akita.Engine
	freq                     akita.Freq
	migrationServiceProvider akita.Port
	pageTableFactory         vm.PageTableFactory
	maxNumReqInFlight        int
	pageWalkingLatency       int
}

// MakeBuilder creates a new builder
func MakeBuilder() Builder {
	return Builder{
		freq:              1 * akita.GHz,
		pageTableFactory:  vm.DefaultPageTableFactory{},
		maxNumReqInFlight: 16,
	}
}

// WithEngine sets the engine to be used with the MMU
func (b Builder) WithEngine(engine akita.Engine) Builder {
	b.engine = engine
	return b
}

// WithFreq sets the frequency that the MMU to work at
func (b Builder) WithFreq(freq akita.Freq) Builder {
	b.freq = freq
	return b
}

// WithMigrationServiceProvider sets the destination port that can perform
// page migration.
func (b Builder) WithMigrationServiceProvider(p akita.Port) Builder {
	b.migrationServiceProvider = p
	return b
}

// WithPageTableFactory sets the page table factory to be used by the MMU
func (b Builder) WithPageTableFactory(f vm.PageTableFactory) Builder {
	b.pageTableFactory = f
	return b
}

// WithMaxNumReqInFlight sets the number of requests can be concurrently
// processed by the MMU.
func (b Builder) WithMaxNumReqInFlight(n int) Builder {
	b.maxNumReqInFlight = n
	return b
}

// WithPageWalkingLatency sets the number of cycles required for walking a page
// table.
func (b Builder) WithPageWalkingLatency(n int) Builder {
	b.pageWalkingLatency = n
	return b
}

// Build returns a newly created MMU component
func (b Builder) Build(name string) *MMUImpl {
	mmu := new(MMUImpl)
	mmu.TickingComponent = *akitaext.NewTickingComponent(
		name, b.engine, b.freq, mmu)

	mmu.pageTableFactory = b.pageTableFactory

	mmu.ToTop = akita.NewLimitNumMsgPort(mmu, 16)
	mmu.MigrationPort = akita.NewLimitNumMsgPort(mmu, 1)
	mmu.MigrationServiceProvider = b.migrationServiceProvider

	mmu.topSender = akitaext.NewBufferedSender(mmu.ToTop, util.NewBuffer(4))

	mmu.PageTables = make(map[ca.PID]vm.PageTable)
	mmu.maxRequestsInFlight = b.maxNumReqInFlight
	mmu.latency = b.pageWalkingLatency

	return mmu
}
