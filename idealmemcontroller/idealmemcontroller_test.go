package idealmemcontroller

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"

	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/ujjalbuet/mem"
)

var _ = Describe("Ideal Memory Controller", func() {

	var (
		mockCtrl      *gomock.Controller
		engine        *MockEngine
		memController *Comp
		port          *MockPort
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())

		engine = NewMockEngine(mockCtrl)
		port = NewMockPort(mockCtrl)

		memController = New("MemCtrl", engine, 1*mem.MB)
		memController.Freq = 1000 * akita.MHz
		memController.Latency = 10
		memController.ToTop = port
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	It("should process read request", func() {
		readReq := mem.ReadReqBuilder{}.
			WithSendTime(10).
			WithDst(memController.ToTop).
			WithAddress(0).
			WithByteSize(4).
			Build()

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&readRespondEvent{}))

		memController.Handle(readReq)
	})

	It("should process write request", func() {
		writeReq := mem.WriteReqBuilder{}.
			WithSendTime(10).
			WithDst(memController.ToTop).
			WithAddress(0).
			WithData([]byte{0, 1, 2, 3}).
			WithDirtyMask([]bool{false, false, true, false}).
			Build()

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&writeRespondEvent{}))

		memController.Handle(writeReq)
	})
	//passed
	It("should handle read respond event", func() {
		data := []byte{1, 2, 3, 4}
		memController.Storage.Write(0, data)

		readReq := mem.ReadReqBuilder{}.
			WithSendTime(10).
			WithDst(memController.ToTop).
			WithAddress(0).
			WithByteSize(4).
			Build()

		event := newReadRespondEvent(11, memController, readReq)

		port.EXPECT().Send(gomock.AssignableToTypeOf(&DataReadyRsp{}))

		memController.Handle(event)
	})
	//passed
	It("should retry read if send DataReady failed", func() {
		data := []byte{1, 2, 3, 4}
		memController.Storage.Write(0, data)

		readReq := mem.ReadReqBuilder{}.
			WithSendTime(10).
			WithDst(memController.ToTop).
			WithAddress(0).
			WithByteSize(4).
			Build()
		event := newReadRespondEvent(11, memController, readReq)

		port.EXPECT().
			Send(gomock.AssignableToTypeOf(&DataReadyRsp{})).
			Return(&akita.SendError{})

		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&readRespondEvent{}))

		memController.Handle(event)
	})

	//passed
	It("should handle write respond event without write mask", func() {
		data := []byte{1, 2, 3, 4}

		writeReq := mem.WriteReqBuilder{}.
			WithSendTime(10).
			WithDst(memController.ToTop).
			WithAddress(0).
			WithData(data).
			Build()
		event := newWriteRespondEvent(11, memController, writeReq)

		port.EXPECT().Send(gomock.AssignableToTypeOf(&WriteDoneRsp{}))

		memController.Handle(event)

		retData, _ := memController.Storage.Read(0, 4)
		Expect(retData).To(Equal([]byte{1, 2, 3, 4}))
	})
	//passed
	It("should handle write respond event", func() {
		memController.Storage.Write(0, []byte{9, 9, 9, 9})
		data := []byte{1, 2, 3, 4}
		dirtyMask := []bool{false, true, false, false}

		writeReq := mem.WriteReqBuilder{}.
			WithSendTime(10).
			WithDst(memController.ToTop).
			WithAddress(0).
			WithData(data).
			WithDirtyMask(dirtyMask).
			Build()
		event := newWriteRespondEvent(11, memController, writeReq)

		port.EXPECT().Send(gomock.AssignableToTypeOf(&WriteDoneRsp{}))

		memController.Handle(event)
		retData, _ := memController.Storage.Read(0, 4)
		//memTS := WriteDoneRsp{}.Mts
		Expect(retData).To(Equal([]byte{9, 2, 9, 9}))
		//Expect(memTS).To(Equal(0))
	})

	//the failing test
	Measure("write with write mask", func(b Benchmarker) {
		data := make([]byte, 64)
		dirtyMask := []bool{
			true, true, true, true, false, false, false, false,
			true, true, true, true, false, false, false, false,
			true, true, true, true, false, false, false, false,
			true, true, true, true, false, false, false, false,
			true, true, true, true, false, false, false, false,
			true, true, true, true, false, false, false, false,
			true, true, true, true, false, false, false, false,
			true, true, true, true, false, false, false, false,
		}
		writeReq := mem.WriteReqBuilder{}.
			WithSendTime(10).
			WithDst(memController.ToTop).
			WithAddress(0).
			WithData(data).
			WithDirtyMask(dirtyMask).
			Build()

		event := newWriteRespondEvent(11, memController, writeReq)
		port.EXPECT().
			Send(gomock.AssignableToTypeOf(&WriteDoneRsp{})).
			AnyTimes()

		b.Time("write time", func() {
			for i := 0; i < 10; i++ { //100000
				memController.Handle(event)
			}
		})
	}, 100)

	It("should retry write respond event, if network busy", func() {
		data := []byte{1, 2, 3, 4}

		writeReq := mem.WriteReqBuilder{}.
			WithSendTime(10).
			WithDst(memController.ToTop).
			WithAddress(0).
			WithData(data).
			Build()
		event := newWriteRespondEvent(11, memController, writeReq)

		port.EXPECT().
			Send(gomock.AssignableToTypeOf(&WriteDoneRsp{})).
			Return(&akita.SendError{})
		engine.EXPECT().
			Schedule(gomock.AssignableToTypeOf(&writeRespondEvent{}))
		memController.Handle(event)
	})
})
