package writeback

import (
	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/akita/akita"
	"gitlab.com/akita/util"
	ca "gitlab.com/akita/util/ca"
	"gitlab.com/ujjalbuet/mem"
	"gitlab.com/ujjalbuet/mem/cache"
)

var _ = Describe("Bottom Parser", func() {
	var (
		mockCtrl        *gomock.Controller
		parser          *bottomParser
		cacheModule     *Cache
		port            *MockPort
		mshr            *MockMSHR
		bankBuf         *MockBuffer
		bottomSender    *MockBufferedSender
		lowModuleFinder *MockLowModuleFinder
		flusherBuffer   *MockBuffer
	)

	BeforeEach(func() {
		mockCtrl = gomock.NewController(GinkgoT())
		port = NewMockPort(mockCtrl)
		mshr = NewMockMSHR(mockCtrl)
		bankBuf = NewMockBuffer(mockCtrl)
		bottomSender = NewMockBufferedSender(mockCtrl)
		lowModuleFinder = NewMockLowModuleFinder(mockCtrl)
		flusherBuffer = NewMockBuffer(mockCtrl)

		builder := Builder{
			WayAssociativity: 4,
			BlockSize:        64,
			ByteSize:         6,
		}

		cacheModule = builder.Build()
		cacheModule.BottomPort = port
		cacheModule.mshr = mshr
		cacheModule.bankBuffers = []util.Buffer{bankBuf}
		cacheModule.bottomSender = bottomSender
		cacheModule.lowModuleFinder = lowModuleFinder
		cacheModule.flusherBuffer = flusherBuffer
		cacheModule.pendingEvictions = nil
		cacheModule.state = cacheStateRunning

		parser = &bottomParser{cache: cacheModule}
	})

	AfterEach(func() {
		mockCtrl.Finish()
	})

	FIt("should return if no req to parse", func() {
		port.EXPECT().Peek().Return(nil)
		ret := parser.Tick(10)
		Expect(ret).To(BeFalse())
	})

	Context("data ready", func() {
		var (
			read           *mem.ReadReq
			write          *mem.WriteReq
			readFromBottom *mem.ReadReq
			block          *cache.Block
			dataReady      *mem.DataReadyRsp
			mshrEntry      *cache.MSHREntry
		)

		BeforeEach(func() {
			read = mem.ReadReqBuilder{}.
				WithSendTime(4).
				WithAddress(0x108).
				WithByteSize(4).
				WithPID(1).
				Build()
			write = mem.WriteReqBuilder{}.
				WithSendTime(5).
				WithAddress(0x104).
				WithPID(1).
				WithData([]byte{1, 2, 3, 4}).
				Build()
			readFromBottom = mem.ReadReqBuilder{}.
				WithSendTime(6).
				WithAddress(0x100).
				WithByteSize(4).
				Build()
			dataReady = mem.DataReadyRspBuilder{}.
				WithSendTime(10).
				WithRspTo(readFromBottom.ID).
				WithData([]byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}).
				Build()

			block = &cache.Block{PID: 1, Tag: 0x100}
			mshrEntry = &cache.MSHREntry{
				Requests: []interface{}{
					&transaction{read: read},
					&transaction{write: write},
				},
				ReadReq: readFromBottom,
				Block:   block,
			}
		})

		It("should stall if bank is busy", func() {
			mshr.EXPECT().AllEntries().Return([]*cache.MSHREntry{mshrEntry})
			port.EXPECT().Peek().Return(dataReady)
			bankBuf.EXPECT().CanPush().Return(false)

			ret := parser.Tick(10)

			Expect(ret).To(BeFalse())
		})

		It("should ignore dataready if corresponding entry is not in MSHR", func() {
			mshr.EXPECT().AllEntries().Return([]*cache.MSHREntry{})
			port.EXPECT().Peek().Return(dataReady)
			port.EXPECT().Retrieve(akita.VTimeInSec(10))

			ret := parser.Tick(10)

			Expect(ret).To(BeTrue())
		})

		It("should send to bank", func() {
			mshr.EXPECT().AllEntries().Return([]*cache.MSHREntry{mshrEntry})
			port.EXPECT().Peek().Return(dataReady)
			port.EXPECT().Retrieve(akita.VTimeInSec(10))
			mshr.EXPECT().Remove(ca.PID(1), uint64(0x100))
			bankBuf.EXPECT().CanPush().Return(true)
			bankBuf.EXPECT().
				Push(gomock.Any()).
				Do(func(trans *transaction) {
					Expect(trans.bankAction).To(Equal(bankWriteFetched))
					Expect(trans.mshrEntry).To(BeIdenticalTo(mshrEntry))
					Expect(mshrEntry.Data).To(Equal([]byte{
						1, 2, 3, 4, 1, 2, 3, 4,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
						1, 2, 3, 4, 5, 6, 7, 8,
					}))
					Expect(block.IsDirty).To(BeTrue())
					Expect(block.DirtyMask).To(Equal([]bool{
						false, false, false, false, true, true, true, true,
						false, false, false, false, false, false, false, false,
						false, false, false, false, false, false, false, false,
						false, false, false, false, false, false, false, false,
						false, false, false, false, false, false, false, false,
						false, false, false, false, false, false, false, false,
						false, false, false, false, false, false, false, false,
						false, false, false, false, false, false, false, false,
					}))
				})

			ret := parser.Tick(10)

			Expect(ret).To(BeTrue())
		})
	})

	Context("done rsp, need fetch", func() {
		var (
			evictionTrans *transaction
			read          *mem.ReadReq
			mshrEntry     *cache.MSHREntry
			evictionReq   *mem.WriteReq
			doneRsp       *mem.WriteDoneRsp
		)

		BeforeEach(func() {
			read = mem.ReadReqBuilder{}.
				WithSendTime(6).
				WithAddress(0x104).
				WithByteSize(4).
				Build()
			evictionReq = mem.WriteReqBuilder{}.
				WithSendTime(8).
				WithAddress(0x200).
				Build()
			mshrEntry = &cache.MSHREntry{}
			evictionTrans = &transaction{
				read:      read,
				eviction:  evictionReq,
				mshrEntry: mshrEntry,
			}
			cacheModule.pendingEvictions = append(
				cacheModule.pendingEvictions, evictionTrans)
			doneRsp = mem.WriteDoneRspBuilder{}.
				WithSendTime(10).
				WithRspTo(evictionReq.ID).
				Build()
		})

		It("should stall if cannot send to bottom", func() {
			port.EXPECT().Peek().Return(doneRsp)
			bottomSender.EXPECT().CanSend(1).Return(false)

			ret := parser.Tick(10)

			Expect(ret).To(BeFalse())
		})

		It("should send fetch request to bottom", func() {
			port.EXPECT().Peek().Return(doneRsp)
			lowModuleFinder.EXPECT().Find(uint64(0x100))
			bottomSender.EXPECT().CanSend(1).Return(true)
			port.EXPECT().Retrieve(gomock.Any())
			var readReq *mem.ReadReq
			bottomSender.EXPECT().Send(gomock.Any()).
				Do(func(fetch *mem.ReadReq) {
					readReq = fetch
					Expect(fetch.Address).To(Equal(uint64(0x100)))
					Expect(fetch.AccessByteSize).To(Equal(uint64(64)))
				})

			ret := parser.Tick(10)

			Expect(ret).To(BeTrue())
			Expect(mshrEntry.ReadReq).To(BeIdenticalTo(readReq))
			Expect(cacheModule.pendingEvictions).To(HaveLen(0))
		})
	})

	Context("done rsp, no fetch", func() {
		var (
			evictionTrans *transaction
			write         *mem.WriteReq
			evictionReq   *mem.WriteReq
			doneRsp       *mem.WriteDoneRsp
			block         *cache.Block
		)

		BeforeEach(func() {
			block = &cache.Block{}
			write = mem.WriteReqBuilder{}.
				WithSendTime(6).
				WithAddress(0x100).
				WithData([]byte{
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
					1, 2, 3, 4, 5, 6, 7, 8,
				}).
				Build()
			evictionReq = mem.WriteReqBuilder{}.
				WithSendTime(8).
				WithAddress(0x200).
				Build()
			evictionTrans = &transaction{
				write:    write,
				block:    block,
				eviction: evictionReq,
			}
			cacheModule.pendingEvictions = append(
				cacheModule.pendingEvictions, evictionTrans)
			doneRsp = mem.WriteDoneRspBuilder{}.
				WithSendTime(10).
				WithRspTo(evictionReq.ID).
				Build()
		})

		It("should stall if cannot send to bank buf", func() {
			port.EXPECT().Peek().Return(doneRsp)
			bankBuf.EXPECT().CanPush().Return(false)

			ret := parser.Tick(10)

			Expect(ret).To(BeFalse())
		})

		It("should send write hit to bank", func() {
			port.EXPECT().Peek().Return(doneRsp)
			bankBuf.EXPECT().CanPush().Return(true)
			bankBuf.EXPECT().Push(gomock.Any())
			port.EXPECT().Retrieve(gomock.Any())

			ret := parser.Tick(10)

			Expect(ret).To(BeTrue())
			Expect(evictionTrans.bankAction).To(Equal(bankWriteHit))
			Expect(cacheModule.pendingEvictions).To(HaveLen(0))
		})
	})

	Context("done for flushing", func() {
		var (
			done *mem.WriteDoneRsp
		)

		BeforeEach(func() {
			cacheModule.state = cacheStateFlushing
			done = &mem.WriteDoneRsp{}
		})

		It("should stall if cannot send to bank buf", func() {
			port.EXPECT().Peek().Return(done)
			flusherBuffer.EXPECT().CanPush().Return(false)

			ret := parser.Tick(10)

			Expect(ret).To(BeFalse())
		})

		It("should send write hit to bank", func() {
			port.EXPECT().Peek().Return(done)
			flusherBuffer.EXPECT().CanPush().Return(true)
			flusherBuffer.EXPECT().Push(gomock.Any())
			port.EXPECT().Retrieve(gomock.Any())

			ret := parser.Tick(10)

			Expect(ret).To(BeTrue())
		})
	})

})
