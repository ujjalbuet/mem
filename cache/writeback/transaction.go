package writeback

import (
	"gitlab.com/ujjalbuet/mem"
	"gitlab.com/ujjalbuet/mem/cache"
)

type bankAction int

const (
	bankActionInvalid bankAction = iota
	bankReadHit
	bankReadPartialHit //added for partial read hit action
	bankWriteHit
	bankReadForEviction
	bankWriteFetched
)

type transaction struct {
	read         *mem.ReadReq
	write        *mem.WriteReq
	block        *cache.Block
	victim       *cache.Block
	evictingAddr uint64
	eviction     *mem.WriteReq
	evictionDone *WriteDoneRsp
	mshrEntry    *MSHREntry
	bankAction   bankAction
	warpts       int //transaction should have warpts info passed to it
	wts          int
	rts          int
}

func (t transaction) req() mem.AccessReq {
	if t.read != nil {
		return t.read
	}
	if t.write != nil {
		return t.write
	}
	return nil
}
