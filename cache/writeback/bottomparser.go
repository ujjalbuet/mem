package writeback

import (
	"log"
	"reflect"

	"gitlab.com/akita/akita"
	"gitlab.com/akita/util/tracing"
	"gitlab.com/ujjalbuet/mem"
)

type bottomParser struct {
	cache *Cache
}

func (p *bottomParser) Tick(now akita.VTimeInSec) bool {
	req := p.cache.BottomPort.Peek()
	if req == nil {
		return false
	}

	switch req := req.(type) {
	case *DataReadyRsp:
		return p.handleDataReady(now, req)
	case *WriteDoneRsp:
		return p.handleDoneRsp(now, req)
	default:
		log.Panicf("cannot handle request of type %s\n", reflect.TypeOf(req))
	}

	return false
}

func (p *bottomParser) handleDataReady(
	now akita.VTimeInSec,
	dr *DataReadyRsp,
) bool {
	mshrEntry := p.findMSHREntry(dr)
	if mshrEntry == nil {
		p.cache.BottomPort.Retrieve(now)
		return true
	}

	block := mshrEntry.Block
	bankNum := bankID(block,
		p.cache.directory.WayAssociativity(), len(p.cache.bankBuffers))

	if !p.cache.bankBuffers[bankNum].CanPush() {
		return false
	}

	p.cache.BottomPort.Retrieve(now)
	mshrEntry.Block.Rts = dr.Rts
	mshrEntry.Block.Wts = dr.Wts
	mshrEntry.DataReady = dr
	p.combineData(mshrEntry)
	trans := &transaction{
		mshrEntry:  mshrEntry,
		bankAction: bankWriteFetched,
	}
	p.cache.bankBuffers[bankNum].Push(trans)

	p.cache.mshr.Remove(block.PID, block.Tag)

	tracing.TraceReqFinalize(mshrEntry.ReadReq, now, p.cache)

	return true
}

func (p *bottomParser) isMshrEntryPresent(dr *mem.DataReadyRsp) bool {
	entries := p.cache.mshr.AllEntries()
	for _, e := range entries {
		if e.ReadReq != nil && dr.RespondTo == e.ReadReq.ID {
			return true
		}
	}
	return false
}

func (p *bottomParser) findMSHREntry(dr *DataReadyRsp) *MSHREntry {
	entries := p.cache.mshr.AllEntries()
	for _, e := range entries {
		if e.ReadReq != nil && dr.RespondTo == e.ReadReq.ID {
			return e
		}
	}
	return nil
}

func (p *bottomParser) combineData(mshrEntry *MSHREntry) {
	mshrEntry.Data = mshrEntry.DataReady.Data
	mshrEntry.Block.DirtyMask = make([]bool, 1<<p.cache.log2BlockSize)
	for _, t := range mshrEntry.Requests {
		trans := t.(*transaction)
		if trans.read != nil {
			continue
		}

		mshrEntry.Block.IsDirty = true
		write := trans.write
		_, offset := getCacheLineID(write.Address, p.cache.log2BlockSize)
		for i := 0; i < len(write.Data); i++ {
			if write.DirtyMask == nil || write.DirtyMask[i] {
				index := offset + uint64(i)
				mshrEntry.Data[index] = write.Data[i]
				mshrEntry.Block.DirtyMask[index] = true
			}
		}
	}
}

func (p *bottomParser) handleDoneRsp(
	now akita.VTimeInSec,
	done *WriteDoneRsp,
) bool {
	if p.cache.state == cacheStateFlushing {
		return p.handleFlushReturn(now, done)
	}

	trans := p.findEvictionTrans(done)
	trans.evictionDone = done
	trans.wts = done.Wts
	trans.rts = done.Rts
	if trans.mshrEntry != nil {
		return p.fetch(now, trans)
	}
	return p.writeBank(now, trans)
}

func (p *bottomParser) handleFlushReturn(
	now akita.VTimeInSec,
	done *WriteDoneRsp,
) bool {
	if !p.cache.flusherBuffer.CanPush() {
		return false
	}
	p.cache.flusherBuffer.Push(done)
	p.cache.BottomPort.Retrieve(now)
	return true
}

func (p *bottomParser) findEvictionTrans(done *WriteDoneRsp) *transaction {
	for _, t := range p.cache.pendingEvictions {
		if t.eviction.ID == done.RespondTo {
			return t
		}
	}
	panic("pending eviction not found")
}

func (p *bottomParser) removeEvictionTrans(done *WriteDoneRsp) {
	for i, t := range p.cache.pendingEvictions {
		if t.eviction.ID == done.RespondTo {
			p.cache.pendingEvictions = append(
				(p.cache.pendingEvictions)[:i],
				(p.cache.pendingEvictions)[i+1:]...)
			return
		}
	}
	panic("pending eviction not found")
}

func (p *bottomParser) fetch(now akita.VTimeInSec, trans *transaction) bool {
	if !p.cache.bottomSender.CanSend(1) {
		return false
	}

	var addr uint64
	if trans.read != nil {
		addr = trans.read.Address
	} else {
		addr = trans.write.Address
	}
	cachelineID, _ := getCacheLineID(addr, p.cache.log2BlockSize)
	dst := p.cache.lowModuleFinder.Find(cachelineID)

	read := mem.ReadReqBuilder{}.
		WithSendTime(now).
		WithSrc(p.cache.BottomPort).
		WithDst(dst).
		WithAddress(cachelineID).
		WithByteSize(1 << p.cache.log2BlockSize).
		Build()
	trans.mshrEntry.ReadReq = read
	p.cache.bottomSender.Send(read)
	p.cache.BottomPort.Retrieve(now)

	p.removeEvictionTrans(trans.evictionDone)

	tracing.TraceReqFinalize(trans.eviction, now, p.cache)
	tracing.TraceReqInitiate(trans.mshrEntry.ReadReq, now, p.cache,
		tracing.MsgIDAtReceiver(trans.req(), p.cache))

	return true
}

func (p *bottomParser) writeBank(
	now akita.VTimeInSec,
	trans *transaction,
) bool {
	bankNum := bankID(trans.block, p.cache.directory.WayAssociativity(),
		len(p.cache.bankBuffers))
	bankBuf := p.cache.bankBuffers[bankNum]
	if !bankBuf.CanPush() {
		return false
	}

	trans.bankAction = bankWriteHit
	bankBuf.Push(trans)
	p.cache.BottomPort.Retrieve(now)

	p.removeEvictionTrans(trans.evictionDone)
	tracing.TraceReqFinalize(trans.eviction, now, p.cache)

	return true
}
